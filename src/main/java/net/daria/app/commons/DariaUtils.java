package net.daria.app.commons;

import java.util.Collection;

/**
 * @author abohaichuk
 */
public class DariaUtils {
    public static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }

    public static boolean isNotEmpty(String s) {
        return !isNullOrEmpty(s);
    }
}
