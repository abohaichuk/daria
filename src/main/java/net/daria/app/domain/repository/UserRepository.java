package net.daria.app.domain.repository;

import net.daria.app.domain.entity.User;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * @author abohaichuk
 */
public interface UserRepository extends PagingAndSortingRepository<User, Long> {
}
