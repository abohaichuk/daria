package net.daria.app.domain.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.daria.app.domain.converter.LocalDateTimeAttributeConverter;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author abohaichuk
 */
@Data
@EqualsAndHashCode(of = {"id"})
@Entity
@Table(name = "users")
public class User implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Convert(converter = LocalDateTimeAttributeConverter.class)
    private LocalDateTime createdTime;
    private String firstName;
    private String lastName;

    public User() {}

    public User(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @PrePersist
    public void prePersist() {
        createdTime = LocalDateTime.now();
    }

}
