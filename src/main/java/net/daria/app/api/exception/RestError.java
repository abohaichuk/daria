package net.daria.app.api.exception;

import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.http.HttpStatus;


/**
 * @author abohaichuk
 */
@Value
@AllArgsConstructor
public class RestError {
    private final HttpStatus status;
    private final int code;
    private final String message;
    private final Throwable throwable;
}
