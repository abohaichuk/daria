package net.daria.app.api.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author abohaichuk
 */
@ControllerAdvice
public class GlobalCtrlExceptionHandler {
    private static final Logger LOG = LoggerFactory.getLogger(GlobalCtrlExceptionHandler.class);

    @ExceptionHandler({NotFoundException.class})
    public ResponseEntity<RestError> handleNotFound(NotFoundException ex) {
        HttpStatus status = AnnotationUtils.findAnnotation(ex.getClass(), ResponseStatus.class).code();
        LOG.error(ex.getMessage());
        return new ResponseEntity<>(new RestError(status, status.value(), ex.getMessage(), ex.getCause()), status);
    }
}
