package net.daria.app.api.resource;

import net.daria.app.api.dto.Parameter;
import net.daria.app.api.dto.UserDto;
import net.daria.app.commons.DariaUtils;
import net.daria.app.domain.entity.User;
import net.daria.app.service.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author abohaichuk
 */
@RestController
@RequestMapping("api/users")
public class UserCtrl {

    @Autowired
    UserService service;

    @Autowired
    ModelMapper mapper;

    @RequestMapping
    public ResponseEntity<List<UserDto>>
    find(@RequestParam(name = "page") int page, @RequestParam(name = "limit") int limit) {
        if (limit <= 0)
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.BAD_REQUEST);
        Pageable pageable = new PageRequest(page, limit, new Sort(Sort.Direction.DESC, "createdTime"));
        Iterable<User> users = service.findAll(pageable);
        return new ResponseEntity<>(convert(users), HttpStatus.OK);
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<UserDto> create(@RequestBody @NotNull UserDto dto) {
        User user = mapper.map(dto, User.class);
        User saved = service.create(user);
        return new ResponseEntity<>(toDto(saved), HttpStatus.CREATED);
    }

    @RequestMapping("/{id}")
    public ResponseEntity<UserDto> findOne(@PathVariable Long id) {
        User user = service.findOne(id);
        return new ResponseEntity<>(toDto(user), HttpStatus.OK);
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.PUT)
    public ResponseEntity<UserDto> update(@RequestBody @NotNull UserDto dto, @PathVariable Long id) {
        User user = toEntity(dto);
        User changed = service.update(id, user);
        return new ResponseEntity<>(toDto(changed), HttpStatus.ACCEPTED);
    }

    @RequestMapping(method = RequestMethod.DELETE)
    public ResponseEntity delete(@RequestBody Parameter<Set<Long>> ids) {
        service.remove(ids.getData());
        return new ResponseEntity(HttpStatus.ACCEPTED);
    }

    private List<UserDto> convert(Iterable<User> users) {
        return StreamSupport.stream(users.spliterator(), false)
                .map(this::toDto)
                .collect(Collectors.toList());
    }

    private UserDto toDto(User user) {
        return user != null ? mapper.map(user, UserDto.class) : null;
    }

    private User toEntity(UserDto dto) {
        return dto != null ? mapper.map(dto, User.class) : null;
    }
}
