package net.daria.app.api.converter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

/**
 * @author abohaichuk
 */
public class JsonDateTimeDeserializer extends JsonDeserializer<LocalDateTime> {
    @Override
    public LocalDateTime deserialize(JsonParser json, DeserializationContext ctxt) throws IOException {
        return new Timestamp(Long.valueOf(json.getText())).toLocalDateTime();
    }
}
