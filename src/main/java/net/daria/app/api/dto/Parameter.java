package net.daria.app.api.dto;

import lombok.AllArgsConstructor;
import lombok.Value;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * @author abohaichuk
 */
@Value
@AllArgsConstructor
public class Parameter<T> implements Serializable {
    @NotNull
    @Valid
    private final T data;
}
