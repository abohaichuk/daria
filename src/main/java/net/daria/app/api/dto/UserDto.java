package net.daria.app.api.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.daria.app.api.converter.JsonDateTimeSerializer;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @author abohaichuk
 */
@Data
@NoArgsConstructor
public class UserDto implements Serializable {
    private Long id;
    private String firstName;
    private String lastName;
    @JsonSerialize(using = JsonDateTimeSerializer.class)
    protected LocalDateTime createdTime;
}
