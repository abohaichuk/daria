package net.daria.app.service;

import net.daria.app.api.exception.NotFoundException;
import net.daria.app.domain.entity.User;
import net.daria.app.domain.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

import static net.daria.app.commons.DariaUtils.isNotEmpty;

/**
 * @author abohaichuk
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository repository;

    @Override
    public User findOne(Long id) {
        return repository.findOne(id);
    }

    @Override
    public Iterable<User> findAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    @Transactional
    @Override
    public User create(User user) {
        return repository.save(user);
    }

    @Transactional
    @Override
    public User update(Long id, User dto) {
        User user = getIfPresent(id);
        if (isNotEmpty(dto.getFirstName()))
            user.setFirstName(dto.getFirstName());
        if (isNotEmpty(dto.getLastName()))
            user.setLastName(dto.getLastName());
        return repository.save(user);
    }

    @Transactional
    @Override
    public void remove(Set<Long> ids) {
        ids.forEach(id -> repository.delete(id));
    }

    private User getIfPresent(Long id) {
        User user = repository.findOne(id);
        if (user == null)
            throw new NotFoundException("not found user with id: "+id);
        return user;
    }

}
