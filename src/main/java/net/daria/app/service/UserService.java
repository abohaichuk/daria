package net.daria.app.service;

import net.daria.app.domain.entity.User;
import org.springframework.data.domain.Pageable;

import java.util.Set;

/**
 * @author abohaichuk
 */
public interface UserService {
    User findOne(Long id);
    Iterable<User> findAll(Pageable pageable);
    User create(User user);
    User update(Long id, User dto);
    void remove(Set<Long> ids);
}
